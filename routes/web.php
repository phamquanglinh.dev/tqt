<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PayPalController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, "indexPageAction"]);
Route::get('/danh-muc/{slug}', [CategoryController::class, "getCategoryBySlugAction", 'slug']);
Route::get('/san-pham/{slug}', [ProductController::class, "getProductBySlugAction", 'slug']);
Route::post('/them-vao-gio-hang', [ProductController::class, "addProductToCardAction"]);
Route::get('/gio-hang', [CartController::class, "cartPageAction"])->name('cart');
Route::post('/tao-don-hang', [CartController::class, "createOrderAction"]);
Route::get('/order/{uuid}', [CartController::class, "orderPageAction"]);
Route::post('/confirm_order/{uuid}', [CartController::class, "confirmOrderPageAction"]);
Route::delete('/delete-item', [CartController::class, "deleteItemFromCart"]);
Route::put('/update-item/{id}', [CartController::class, "updateItemFromCart"]);


Route::get('/paypal/create-payment', [PayPalController::class, 'createPayment']);
Route::get('/paypal/execute-payment', [PayPalController::class, 'executePayment']);
Route::get('/paypal/cancel-payment', [PayPalController::class, 'cancelPayment'])->name('paypal.cancel');
