<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,600;1,400&display=swap" rel="stylesheet">

    <title>TQT Shop - @yield("title")</title>


    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/bootstrap.min.css")}}">

    <link rel="stylesheet" type="text/css" href="{{asset("assets/css/font-awesome.css")}}">

    <link rel="stylesheet" href="{{asset("assets/css/templatemo-hexashop.css")}}">

    <link rel="stylesheet" href="{{asset("assets/css/owl-carousel.css")}}">

    <link rel="stylesheet" href="{{asset("assets/css/lightbox.css")}}">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">

</head>

<body>

<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->
<!-- ***** Header Area Start ***** -->
@include("components.menu")
<!-- ***** Header Area End ***** -->

@yield("content")

@include("components.footer")
@stack('after_scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
@if(session("succeed"))
    <script>
        Toastify({
            text: "{{session('succeed')}}",
            duration: 1000,
            close: true,
            gravity: "top", // `top` or `bottom`
            position: "right", // `left`, `center` or `right`
            stopOnFocus: true, // Prevents dismissing of toast on hover
            style: {
                background: "linear-gradient(to right, #00b09b, #96c93d)",
            },
            onClick: function () {
            } // Callback after click
        }).showToast();
    </script>
@endif
</body>
</html>
