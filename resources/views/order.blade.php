@php
    use App\ViewModel\Cart\OrderViewModel;
    /**
    * @var OrderViewModel $orderViewModel
    */
@endphp
@extends('layouts.app')
@section('content')
    <div style="height: 150px"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 p-1">
                <div class="border p-2 rounded">
                    <div class="font-weight-bold mb-2">Thông tin người nhận</div>
                    <div class="mb-1">Tên người đặt hàng : {{$orderViewModel->getOrder()->getCustomerName()}}</div>
                    <div class="mb-1">Email : {{$orderViewModel->getOrder()->getEmail()}}</div>
                    <div class="mb-1">Số điện thoại : {{$orderViewModel->getOrder()->getPhone()}}</div>
                    <div class="mb-1">Địa chỉ : {{$orderViewModel->getOrder()->getAddress()}}</div>
                </div>
                <div class="border p-2 rounded mt-3">
                    @foreach($orderViewModel->getOrder()->getCartItems() as $item)
                        <div>
                            <span class="font-weight-bold">{{$item->getProductName()}}</span>
                            x <span class="mr-2">{{$item->getQuantity()}}</span>
                            <span>( {{number_format($item->getPrice())}} đ )</span>
                        </div>
                    @endforeach
                    <div class="mt-3 border-top pt-2">
                        <span class="font-weight-bold">Thành tiền</span>
                        : {{number_format($orderViewModel->getOrder()->getTotal())}} đ
                    </div>
                </div>
            </div>
            <div class="col-md-4 p-1">
                <form action="{{url('/confirm_order/'.$orderViewModel->getOrder()->getUuid())}}" method="post">
                    @csrf
                    <div class="border p-2 rounded">
                        <label>Phương thức chuyển tiền</label>
                        <select id="payment_method" name="payment_method" class="form-control">
                            <option value="bank" selected>Chuyển khoản ngân hàng</option>
                            <option value="cash">Thanh toán khi nhận hàng</option>
                        </select>
                        <div class="my-2">
                            <img id="qr-code" src="{{$orderViewModel->getOrder()->getQRCode()}}" class="w-100">
                        </div>
                        <div class="my-3 text-success small">* Khi xác nhận đơn hàng, hê thống sẽ kiểm tra đơn hàng và
                            gửi mail thông tin đơn hàng cho bạn qua email
                        </div>
                        <button class="my-2 btn btn-success w-100" type="submit">Xác nhận đơn hàng</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
    <script>
        $("#payment_method").change(function (e) {
            switch ($(this).val()) {
                case "cash":
                    $("#qr-code").hide()
                    break;
                case "bank":
                    $("#qr-code").show()
                    break;
                default:
                    $("#qr-code").hide()
            }
        })
    </script>
@endpush
