@php
    use App\ViewModel\Index\IndexViewModel;
    /**
* @var IndexViewModel $indexViewModel
 */
   $pinCategories =  $indexViewModel->getPinCategories();
@endphp
<div class="main-banner" id="top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="left-content">
                    <div class="thumb">
                        <div class="inner-content">
                            <h4>TQT Store</h4>
                            <span>Awesome, clean &amp; creative HTML5 Template</span>
                            <div class="main-border-button">
                                <a href="#">Mua ngay!</a>
                            </div>
                        </div>
                        <img src="{{asset("assets/images/left-banner-image.jpg")}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="right-content">
                    <div class="row">
                        @foreach($pinCategories as $category)
                            <div class="col-lg-6">
                                <div class="right-first-image">
                                    <div class="thumb">
                                        <div class="inner-content">
                                            <h4>{{$category->getName()}}</h4>
                                        </div>
                                        <div class="hover-content">
                                            <div class="inner">
                                                <h4>{{$category->getName()}}</h4>
                                                <div class="main-border-button">
                                                    <a href="{{url("/danh-muc/".$category->getSlug())}}">Xem thêm</a>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{asset($category->getThumbnail())}}">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
