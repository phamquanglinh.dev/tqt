@php
    use Products\Object\ProductsListObject;
    /**
* @var ProductsListObject[] $products
 */
@endphp
@foreach($products as $product)
    <div class="item">
        <div class="thumb">
            <div class="hover-content">
                <ul>
                    <li><a href="single-product.html"><i class="fa fa-eye"></i></a></li>
                    {{--                    <li><a href="single-product.html"><i class="fa fa-star"></i></a></li>--}}
                    <li><a href="single-product.html"><i class="fa fa-shopping-cart"></i></a>
                    </li>
                </ul>
            </div>
            <img src="{{$product->getPrimaryThumbnail()}}" alt="">
        </div>
        <div class="down-content">
            <h4>{{$product->getName()}}</h4>
            <span>{{number_format($product->getPromotePrice() ?? $product->getOriginalPrice())}} đ</span>
        </div>
    </div>
@endforeach
