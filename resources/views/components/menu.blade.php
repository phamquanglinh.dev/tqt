@php
    use App\ViewModel\Index\HeaderViewModel;
    /**
* @var HeaderViewModel $headerViewModel
 */

@endphp
<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="{{url('/')}}" class="logo">
                        <img alt="" src="{{asset("assets/images/logo.png")}}">
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li class="scroll-to-section"><a href="{{url('/')}}" class="">Trang chủ</a></li>
                        {{--                        <li class="scroll-to-section"><a href="#men">Men's</a></li>--}}
                        {{--                        <li class="scroll-to-section"><a href="#women">Women's</a></li>--}}
                        {{--                        <li class="scroll-to-section"><a href="#kids">Kid's</a></li>--}}
                        <li class="submenu">
                            <a href="javascript:;">Danh mục sản phẩm</a>
                            <ul>
                                @foreach($headerViewModel->getCategories() as $category)
                                    <li>
                                        <a href="{{url('/danh-muc/'.$category->getSlug())}}">{{$category->getName()}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="scroll-to-section">
                            <a href="{{url('/')}}" class="">Giới thiệu</a></li>
                        </li>
                        <li class="scroll-to-section" style="position: relative">
                            <span class="badge badge-danger"
                                  style="position: absolute;right: -0.3rem;top: -0.3rem">{{$headerViewModel->getBuyItemCount()}}</span>
                            <a href="{{url('gio-hang')}}"><i class="fa fa-cart-plus p-2 bg-dark text-white rounded-circle" style="font-size: 20px!important;"></i></a>
                        </li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
