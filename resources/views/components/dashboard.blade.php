@php
    use App\ViewModel\Admin\DashboardViewModel;
    /**
     * @var DashboardViewModel $dashboardViewModel
     */
@endphp

@extends(backpack_view('blank'))

@section('content')
    <div class="container-fluid">
        <div name="widget_477265610" class="row mt-3">
            <div class="col-sm-6 col-lg-3">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-success ">
                        <i class="la la-user fs-3 text-success"></i>
                    </div>

                    <div class="card-status-start bg-success"></div>

                    <div class="card-body">
                        <div class="subheader">Sản phẩm</div>

                        <div class="h1 mb-3 ">{{$dashboardViewModel->getTotalProducts()}}</div>
                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-danger ">
                        <i class="la la-user fs-3 text-danger"></i>
                    </div>

                    <div class="card-status-start bg-danger"></div>

                    <div class="card-body">
                        <div class="subheader">Đơn hàng</div>

                        <div class="h1 mb-3 ">{{$dashboardViewModel->getTotalOrders()}}</div>
                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-primary ">
                        <i class="la la-user fs-3 text-primary"></i>
                    </div>

                    <div class="card-status-start bg-primary"></div>

                    <div class="card-body">
                        <div class="subheader">Đơn hàng thành công</div>

                        <div class="h1 mb-3 ">{{$dashboardViewModel->getTotalOrderSucceed()}}</div>
                    </div>

                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-success ">
                        <i class="la la-user fs-3 text-success"></i>
                    </div>

                    <div class="card-status-start bg-success"></div>

                    <div class="card-body">
                        <div class="subheader">Doanh thu</div>

                        <div class="h1 mb-3 ">{{number_format($dashboardViewModel->getIncome())}} đ</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
