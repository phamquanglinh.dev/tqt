@php
    use App\ViewModel\Products\Object\ProductsListObject;
    /**
* @var ProductsListObject $products
 */
@endphp

<div class="item">
    <div class="thumb">
        <div class="hover-content">
            <ul class="d-flex justify-content-center">
                <li><a href="{{url('/san-pham/'.$product->getSlug())}}"><i class="fa fa-eye"></i></a></li>
                <li>
                    <form action="{{url('/them-vao-gio-hang')}}" method="post">
                        @csrf
                        <input name="product" value="{{$product->getId()}}" hidden>
                        <input name="quantity" value="1" hidden>
                        <button class="border-0 p-3" style="width: 50px;height: 50px" type="submit"><i class="fa fa-shopping-cart"></i></button>
                    </form>
                </li>
            </ul>
        </div>
        <img src="{{$product->getPrimaryThumbnail()}}" alt="">
    </div>
    <div class="down-content">
        <h4>{{$product->getName()}}</h4>
        <span>{{number_format($product->getPromotePrice() ?? $product->getOriginalPrice())}} đ</span>
    </div>
</div>

