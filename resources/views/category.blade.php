@php
    use Category\CategoryViewModel;
    /**
* @var CategoryViewModel $categoryViewModel
*/
   $category = $categoryViewModel->getCategory();
$paginate  = $categoryViewModel->getCategory()->getPaginate();

@endphp
@extends("layouts.app")
@section('content')
    <section class="section mt-5" id="products">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h2>{{$category->getName()}}</h2>
                        <span>Xem tất cả các sản phẩm {{$category->getName()}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @foreach($category->getProducts() as $product)
                    <div class="col-lg-4">
                        @include("components.product-list-item",['product' => $product])
                    </div>
                @endforeach
                <div class="col-lg-12">
                    <div class="pagination">
                        <ul>
                            @foreach($paginate as $page)
                                <li class="{{$page['active']?"active":""}}">
                                    <a href="{{$page['url']}}">{{$page['label']}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
