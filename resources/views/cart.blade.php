@php
    use App\ViewModel\Cart\CartViewModel;
    /**
* @var CartViewModel $cartViewModel
 */

@endphp
@extends('layouts.app')
@section('content')
    <div style="height: 130px"></div>
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-md-9">
                <h2 class="mb-3">Giỏ Hàng</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Sản Phẩm</th>
                        <th scope="col">Số Lượng</th>
                        <th scope="col">Giá</th>
                        <th scope="col">Thành Tiền</th>
                        <th scope="col">Xóa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cartViewModel->getCartItem() as $key => $cartItem)
                        <tr>
                            <th scope="row">{{$key}}</th>
                            <td>{{$cartItem->getProductName()}}</td>
                            <td >
                                <form action="{{"update-item/".$cartItem->getId()}}" class="d-flex" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="subtract bg-dark p-1 text-center text-white"
                                         style="width: 2rem;cursor:pointer">
                                        -
                                    </div>
                                    <input name="quantity" class="rounded-0 border-0 inputNumber" type="number"
                                           style="width: 3rem;text-align: center"
                                           value="{{$cartItem->getQuantity()}}">
                                    <div class="add bg-dark p-1 text-center text-white" style="width: 2rem;cursor:pointer">
                                        +
                                    </div>
                                    <button type="submit" class="bg-success border-0 p-1 text-center text-white ml-1"
                                         style="width: 2rem;cursor:pointer">
                                        <i class="fa fa-check"></i>
                                    </button>
                                </form>
                            </td>
                            <td>{{number_format($cartItem->getPrice())}} đ</td>
                            <td>{{number_format($cartItem->getMoney())}} đ</td>
                            <td>
                                <form action="{{url('/delete-item')}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <input type="text" name="id" value="{{$cartItem->getId()}}" hidden="">
                                    <button class="btn btn-danger btn-sm">Xóa</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    <!-- Thêm các dòng khác nếu cần -->
                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <h2 class="mb-3">Thông Tin Thanh Toán</h2>
                <form method="post" action="{{url('/tao-don-hang')}}" class="border p-2 rounded shadow py-3">
                    @csrf
                    <div class="form-group">
                        <label for="name">Họ và Tên:</label>
                        <input name="customer_name" type="text" class="form-control" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" name="email" class="form-control" id="email" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Số điện thoại:</label>
                        <input type="text" name="phone" class="form-control" id="phone" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Địa Chỉ:</label>
                        <textarea name="address" class="form-control" id="address" rows="3" required></textarea>
                    </div>
                    <div class="my-2 text-success font-weight-bold">
                        Số tiền phải thanh toán : {{number_format($cartViewModel->getTotalAmount())}} đ
                    </div>
                    <button type="submit" class="btn btn-primary">Tạo đơn hàng</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
    <script>
        // JavaScript để thêm tính năng cho nút "+" và "-"
        const addButtons = document.querySelectorAll('.add');
        const subtractButtons = document.querySelectorAll('.subtract');
        const inputNumbers = document.querySelectorAll('.inputNumber');

        function updateInputValue(index) {
            const currentValue = parseFloat(inputNumbers[index].value);
            if (currentValue < 1) {
                inputNumbers[index].value = 1;
            }
        }

        inputNumbers.forEach((input, index) => {
            input.addEventListener('input', () => {
                updateInputValue(index);
            });
        });

        addButtons.forEach((button, index) => {
            button.addEventListener('click', () => {
                const currentValue = parseFloat(inputNumbers[index].value);
                inputNumbers[index].value = currentValue + 1;
            });
        });

        subtractButtons.forEach((button, index) => {
            button.addEventListener('click', () => {
                const currentValue = parseFloat(inputNumbers[index].value);
                if (currentValue > 1) {
                    inputNumbers[index].value = currentValue - 1;
                }
            });
        });
    </script>
@endpush
