@php
    use Products\ProductViewModel;
    /**
* @var ProductViewModel $productViewModel
 */
    $product = $productViewModel->getProduct()
@endphp
@extends('layouts.app')
@section("content")
    <div style="height: 100px"></div>
    <section class="section mt-5" id="product">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-images">
                        <img style="aspect-ratio: 1" src="{{$product->getPrimaryThumbnail()}}" alt="">
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="right-content">
                        <h4>{{$product->getName()}}</h4>
                        <div class="d-flex my-2 align-items-center ">
                            <div class="mr-2 text-muted midline">{{number_format($product->getOriginalPrice())}} đ</div>
                            <div class="h3 text-warning font-weight-bold">{{number_format($product->getPromotePrice())}}
                                đ
                            </div>
                        </div>
                        <span>{{$product->getDescription()}}</span>
                        <form action="{{url('them-vao-gio-hang')}}" method="POST">
                            @csrf
                            <input hidden name="product" value="{{$product->getId()}}">
                            <div class="quantity-content">
                                <div class="left-content">
                                    <h6 class="text-dark">Số lượng</h6>
                                </div>
                                <div class="right-content">
                                    <div class="quantity buttons_added">
                                        <div class="d-flex justify-content-center align-items-center">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn rounded-0 btn-outline-secondary"
                                                        onclick="decrement()">-
                                                </button>
                                            </div>
                                            <input name="quantity" type="number"
                                                   class="rounded-0 form-control text-center"
                                                   style="width: 3rem"
                                                   id="quantity" value="1"
                                                   min="1"
                                                   onkeyup="updateTotal()"
                                                   required>
                                            <div class="input-group-append">
                                                <button type="button" class="btn rounded-0 btn-outline-secondary"
                                                        onclick="increment()">+
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="d-flex align-items-center">
                                    <h4 class="mr-2">Tổng:</h4>
                                    <h4 id="total"
                                        class="text-success">{{number_format($product->getPromotePrice()??$product->getOriginalPrice())}}
                                        đ</h4>
                                </div>
                                <button type="submit" class="btn btn-success text-white rounded-0 my-2">Thêm vào giỏ
                                    hàng
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .midline {
            position: relative;
            text-decoration: none;
        }

        .midline::before {
            content: "";
            position: absolute;
            bottom: 50%;
            left: 0;
            right: 0;
            border-bottom: 1px solid #000; /* Màu sắc của midline */
        }
    </style>
@endsection
@push("after_scripts")
    <script>
        const pricePerItem = {{$product->getPromotePrice() ?? $product->getOriginalPrice()}}; // Giá tiền cho mỗi sản phẩm

        function updateTotal() {
            const quantityInput = document.getElementById('quantity');
            const totalInput = document.getElementById('total');
            let quantity = parseInt(quantityInput.value);

            if (quantity < 1 || !quantity) {
                quantity = 1;
                quantityInput.value = 1
            }
            const total = quantity * pricePerItem;
            totalInput.innerText = total.toLocaleString('vi-VN', {style: 'currency', currency: 'VND'});
        }

        function increment() {
            const quantityInput = document.getElementById('quantity');
            quantityInput.value = parseInt(quantityInput.value) + 1;
            updateTotal();
        }

        function decrement() {
            var quantityInput = document.getElementById('quantity');
            var newValue = parseInt(quantityInput.value) - 1;
            if (newValue >= 1) {
                quantityInput.value = newValue;
                updateTotal();
            }
        }
    </script>
@endpush
