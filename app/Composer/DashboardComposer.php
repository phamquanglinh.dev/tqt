<?php

namespace App\Composer;

use App\Models\Order;
use App\Models\Product;
use App\ViewModel\Admin\DashboardViewModel;
use Illuminate\View\View;

class DashboardComposer
{
    public function compose(View $view): void
    {
        $totalProduct = Product::query()->count();
        $totalOrder = Order::query()->count();
        $totalOrderSucceed = Order::query()->where('status', 3)->count();
        $income = Order::query()->where('status', 3)->sum('total');

        $view->with('dashboardViewModel', new DashboardViewModel(
            total_products: $totalProduct,
            total_orders: $totalOrder,
            total_order_succeed: $totalOrderSucceed,
            income: $income));
    }
}
