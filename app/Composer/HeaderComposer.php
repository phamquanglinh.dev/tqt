<?php

namespace App\Composer;

use App\Models\BuyItem;
use App\Models\Category;
use App\ViewModel\Index\HeaderViewModel;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use Illuminate\View\View;

class HeaderComposer
{
    public function compose(View $view): void
    {
        $uuid = Cookie::get('anonymous_uuid');

        if (!$uuid) {
            $uuid = Str::uuid();
            Cookie::queue('anonymous_uuid', $uuid, 60 * 24 * 365 * 10);
        }

        $buyItemCount = BuyItem::query()->where('anonymous_uuid', $uuid)->where('order_id', null)->count();

        $categories = Category::query()->limit(20)->get();

        $headerViewModel = new HeaderViewModel(buyItemCount: $buyItemCount, categories: $categories);

        $view->with(['headerViewModel' => $headerViewModel]);
    }
}
