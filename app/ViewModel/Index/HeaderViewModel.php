<?php

namespace App\ViewModel\Index;

use App\Models\Category;
use App\ViewModel\Category\Object\CategoryObject;
use Illuminate\Database\Eloquent\Collection;

class HeaderViewModel
{
    public function __construct(
        private readonly int              $buyItemCount,
        private readonly Collection|array $categories
    )
    {
    }

    /**
     * @return CategoryObject[]
     */
    public function getCategories(): array
    {
        return $this->categories->map(fn(Category $category) => new CategoryObject(
            name: $category['name'], slug: $category['slug'], thumbnail: $category['thumbnail'], products: []
        ))->toArray();
    }

    public function getBuyItemCount(): int
    {
        return $this->buyItemCount;
    }
}
