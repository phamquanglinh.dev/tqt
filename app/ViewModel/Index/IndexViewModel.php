<?php

namespace App\ViewModel\Index;

use App\Models\Category;
use App\ViewModel\Index\Object\CategoryObject;
use App\ViewModel\Index\Object\LatestCategoryObject;
use Illuminate\Database\Eloquent\Collection;

class IndexViewModel
{
    public function __construct(
        private readonly Collection|array $pinCategories,
        private readonly Collection|array $latestCategory
    )
    {
    }

    /**
     * @return CategoryObject[]
     */
    public function getPinCategories(): array
    {
        return $this->pinCategories->map(fn(Category $category) => new CategoryObject(
            name: $category['name'],
            slug: $category['slug'],
            thumbnail: $category['thumbnail']
        ))->toArray();
    }

    /**
     * @return LatestCategoryObject[]
     */
    public function getLatestCategory(): array
    {
        return $this->latestCategory->map(fn(Category $category) => new LatestCategoryObject(
            name: $category['name'],
            slug: $category['slug'],
            products: $category['Products']
        ))->toArray();
    }
}
