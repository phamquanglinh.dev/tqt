<?php

namespace App\ViewModel\Index\Object;

class CategoryObject
{
    public function __construct(
        readonly private string $name,
        readonly private string $slug,
        readonly private string $thumbnail,
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }
}
