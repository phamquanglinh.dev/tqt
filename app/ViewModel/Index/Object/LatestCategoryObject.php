<?php

namespace App\ViewModel\Index\Object;

use App\Models\Product;
use App\ViewModel\Products\Object\ProductsListObject;
use Illuminate\Database\Eloquent\Collection;


class LatestCategoryObject
{
    public function __construct(
        private readonly string           $name,
        private readonly string           $slug,
        private readonly Collection|array $products
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return ProductsListObject[]
     */
    public function getProducts(): array
    {
        return $this->products->map(fn(Product $product) => new ProductsListObject(
            id: $product['id'],
            primary_thumbnail: $product['primary_thumbnail'],
            name: $product['name'],
            original_price: $product['original_price'],
            promote_price: $product['promote_price'],
            slug: $product['slug']
        ))->toArray();
    }
}
