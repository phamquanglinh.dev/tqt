<?php

namespace App\ViewModel\Products;

use App\Models\Product;
use App\ViewModel\Products\Object\ProductObject;


class ProductViewModel
{
    public function __construct(
        private readonly Product $product
    )
    {
    }

    public function getProduct(): ProductObject
    {
        return new ProductObject(
            id:$this->product['id'],
            name: $this->product['name'],
            original_price:  $this->product['original_price'],
            promote_price: $this->product['promote_price'],
            description:  $this->product['description'],
            primary_thumbnail:  $this->product['primary_thumbnail'],
            thumbnails:  $this->product['thumbnails'],
            custom_fields:  $this->product['custom_fields']
        );
    }
}
