<?php

namespace App\ViewModel\Products\Object;

class ProductObject
{
    public function __construct(
        private readonly int $id,
        private readonly string $name,
        private readonly string $original_price,
        private readonly string $promote_price,
        private readonly string $description,
        private readonly string $primary_thumbnail,
        private readonly ?array  $thumbnails,
        private readonly ?array  $custom_fields
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOriginalPrice(): string
    {
        return $this->original_price;
    }

    public function getPromotePrice(): string
    {
        return $this->promote_price;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPrimaryThumbnail(): string
    {
        return $this->primary_thumbnail;
    }

    public function getThumbnails(): ?array
    {
        return $this->thumbnails;
    }

    public function getCustomFields(): ?array
    {
        return $this->custom_fields;
    }
}
