<?php

namespace  App\ViewModel\Products\Object;

class ProductsListObject
{
    public function __construct(
        private readonly int $id,
        private readonly string $primary_thumbnail,
        private readonly string $name,
        private readonly int    $original_price,
        private readonly ?int   $promote_price,
        private readonly string $slug,
    )
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getPrimaryThumbnail(): string
    {
        return $this->primary_thumbnail;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getOriginalPrice(): int
    {
        return $this->original_price;
    }

    public function getPromotePrice(): ?int
    {
        return $this->promote_price;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}
