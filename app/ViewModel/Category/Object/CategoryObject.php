<?php

namespace App\ViewModel\Category\Object;

use App\Models\Product;
use App\ViewModel\Products\Object\ProductsListObject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;


class CategoryObject
{
    public function __construct(
        private readonly string                                $name,
        private readonly string                                $slug,
        private readonly string                                $thumbnail,
        private readonly array|Collection|LengthAwarePaginator $products,
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @return ProductsListObject[]
     */
    public function getProducts(): array
    {
        return $this->products->map(fn(Product $product) => new ProductsListObject(
            id:$product['id'],
            primary_thumbnail: $product['primary_thumbnail'],
            name: $product['name'],
            original_price: $product['original_price'],
            promote_price: $product['promote_price'],
            slug: $product['slug']
        ))->toArray();
    }

    public function getPaginate(): array
    {
        $links = $this->products->toArray()['links'];
        foreach ($links as $key => $link) {
            if (str_contains($link['label'], "re")) {
                $links[$key]['label'] = "<<";
            }
            if (str_contains($link['label'], "xt")) {
                $links[$key]['label'] = ">>";
            }
        }
        return $links;
    }
}
