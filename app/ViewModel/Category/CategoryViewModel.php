<?php

namespace App\ViewModel\Category;

use App\Models\Category;
use App\ViewModel\Category\Object\CategoryObject;


class CategoryViewModel
{
    public function __construct(
        private readonly Category $category
    )
    {
    }

    /**
     * @return CategoryObject
     */
    public function getCategory(): CategoryObject
    {
        $category = $this->category;

        return new CategoryObject(
            name: $category['name'],
            slug: $category['slug'],
            thumbnail: $category['thumbnail'],
            products: $category->Products()->orderBy("created_at", "DESC")->paginate(18)
        );
    }
}
