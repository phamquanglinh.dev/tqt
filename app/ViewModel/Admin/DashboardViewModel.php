<?php

namespace App\ViewModel\Admin;

class DashboardViewModel
{
    public function __construct(
        private readonly int $total_products,
        private readonly int $total_orders,
        private readonly int $total_order_succeed,
        private readonly int $income,

    )
    {
    }

    /**
     * @return int
     */
    public function getTotalProducts(): int
    {
        return $this->total_products;
    }

    /**
     * @return int
     */
    public function getTotalOrders(): int
    {
        return $this->total_orders;
    }

    /**
     * @return int
     */
    public function getTotalOrderSucceed(): int
    {
        return $this->total_order_succeed;
    }

    /**
     * @return int
     */
    public function getIncome(): int
    {
        return $this->income;
    }
}
