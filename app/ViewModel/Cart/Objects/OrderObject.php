<?php

namespace App\ViewModel\Cart\Objects;

use App\Models\BuyItem;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Collection;

class OrderObject
{
    public function __construct(
        private readonly string           $customer_name,
        private readonly ?string          $payment_method,
        private readonly ?string          $phone,
        private readonly ?string          $email,
        private readonly int              $total,
        private readonly int              $status,
        private readonly string           $address,
        private readonly Collection|array $cartItems,
        private readonly string           $uuid,
    )
    {
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return CartItem[]
     */
    public function getCartItems(): array
    {
        return $this->cartItems->map(fn(BuyItem $buyItem) => new CartItem(
            id: $buyItem['id'],
            product_name: $buyItem['product_name'],
            quantity: $buyItem['quantity'],
            price: $buyItem->product->promote_price ?? $buyItem->product->original_price
        ))->toArray();
    }

    /**
     * @return string
     */
    public function getCustomerName(): string
    {
        return $this->customer_name;
    }


    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->payment_method;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @throws GuzzleException
     */
    public function getQRCode(): string
    {
        $client = new Client();

        $response = $client->post("https://api.vietqr.io/v2/generate", [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode([
                'accountNo' => '0327841612',
                'accountName' => 'PHAM QUANG LINH',
                'acqId' => '970432',
                'addInfo' => 'TQT Order ID ' . $this->uuid,
                'amount' => $this->total,
                'template' => 'print'
            ])
        ]);

        $responseData = json_decode($response->getBody(), 1);

        return $responseData['data']['qrDataURL'];
    }
}
