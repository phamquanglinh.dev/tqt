<?php

namespace App\ViewModel\Cart\Objects;

class CartItem
{
    public function __construct(
        private readonly int $id,
        private readonly string $product_name,
        private readonly int    $quantity,
        private readonly int    $price,

    )
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->product_name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->quantity * $this->price;
    }
}
