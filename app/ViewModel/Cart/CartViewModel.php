<?php

namespace App\ViewModel\Cart;

use App\Models\BuyItem;
use App\ViewModel\Cart\Objects\CartItem;
use Illuminate\Database\Eloquent\Collection;

class CartViewModel
{
    public function __construct(
        private readonly Collection|array $buyItems,

    )
    {
    }

    /**
     * @return CartItem[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 03/12/2023 10:16 pm
     */
    public function getCartItem(): array
    {
        return $this->buyItems->map(fn(BuyItem $buyItem) => new CartItem(
            id:$buyItem['id'],
            product_name: $buyItem['product_name'],
            quantity: $buyItem['quantity'],
            price: $buyItem->product->promote_price ?? $buyItem->product->original_price
        ))->toArray();
    }

    /**
     * @return int
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 03/12/2023 10:24 pm
     */
    public function getTotalAmount(): int
    {
        $total = 0;
        foreach ($this->getCartItem() as $cartItem) {
            $total += $cartItem->getMoney();
        }
        return $total;
    }
}
