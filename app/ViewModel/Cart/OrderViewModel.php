<?php

namespace App\ViewModel\Cart;

use App\Models\BuyItem;
use App\Models\Order;
use App\ViewModel\Cart\Objects\OrderObject;

class OrderViewModel
{
    public function __construct(
        private readonly Order $order
    )
    {
    }

    /**
     * @return OrderObject
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 03/12/2023 11:02 pm
     */
    public function getOrder(): OrderObject
    {
        $buyItem = BuyItem::query()->where('order_id', $this->order['id'])->get();
        return new OrderObject(
            customer_name: $this->order['customer_name'],
            payment_method: $this->order['payment_method'],
            phone: $this->order['phone'],
            email: $this->order['email'],
            total: $this->order['total'],
            status: $this->order['status'],
            address: $this->order['address'],
            cartItems: $buyItem,
            uuid: $this->order['uuid']
        );
    }
}
