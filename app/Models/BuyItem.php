<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Product $product
 */
class BuyItem extends Model
{
    use HasFactory;

    protected $table = 'buy_items';
    protected $guarded = ['id'];

    public function Product()
    {
      return  $this->belongsTo(Product::class, "product_id", "id");
    }
}
