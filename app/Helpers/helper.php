<?php

// Tệp: app/Helpers/MyHelper.php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

if (!function_exists('backpack_pro')) {
    function backpack_pro(): bool
    {
        return true;
    }
}


if (!function_exists('base64_upload')) {
    function base64_upload($param, $folder): string
    {
        if (!str_contains($param, "base64")) {
            return $param;
        }

        list($extension, $content) = explode(';', $param);

        $tmpExtension = explode('/', $extension);


        preg_match('/.([0-9]+) /', microtime(), $m);

        $fileName = Str::random(14) . ".$tmpExtension[1]";

        $content = explode(',', $content)[1];

        $storage = Storage::disk('upload');

        $checkDirectory = $storage->exists($folder);

        if (!$checkDirectory) {
            $storage->makeDirectory($folder);
        }
        $storage->put($folder . '/' . $fileName, base64_decode($content), 'public');

        return "/uploads/".$folder . "/" . $fileName;
    }
}
