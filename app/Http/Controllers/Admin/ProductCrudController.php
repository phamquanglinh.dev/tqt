<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('Sản phẩm', 'Danh sách sản phẩm');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('category')->label('Danh mục sản phẩm');
        CRUD::column('name')->label('Tên sản phẩm');
        CRUD::column('original_price')->label('Giá gốc');
        CRUD::column('promote_price')->label('Giá KM');
        CRUD::column('primary_thumbnail')->label('Ảnh bìa')->type('image');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        CRUD::field('category_id');
        CRUD::field('name')->label('Tên sản phẩm');
        CRUD::field('slug')->type('hidden')->default('');
        CRUD::field('description')->type('textarea')->label('Mô tả sản phẩm');
        CRUD::field('original_price')->type('number')->label('Giá gốc')->wrapper([
            'class' => 'col-md-6 mb-3'
        ])->suffix('đ');
        CRUD::field('promote_price')->label('Giá KM')->wrapper([
            'class' => 'col-md-6 mb-3'
        ])->suffix('đ');

        CRUD::addField([
            'name' => 'primary_thumbnail',
            'label' => 'Ảnh SP chính',
            'type' => 'image',
            'crop' => true,
            'upload' => true,

            'aspect_ratio' => 1
        ]);

        CRUD::field('thumbnails')->label('Ảnh SP thêm')->type('repeatable')->fields([
            [
                'name' => 'thumbnail',
                'label' => false,
                'type' => 'image',
                'crop' => 'true',
                'upload' => true,
                'disk' => 'upload',
                'aspect_ratio' => 1
            ]
        ])->new_item_label('Thêm ảnh')->init_rows(0)->max_rows(5);
        CRUD::field('custom_fields')->label('Thuộc tính tự nhập')->type('repeatable')->fields([
            [
                'name' => 'label',
                'label' => 'Tên thuộc tính',
                'wrapper' => [
                    'class' => 'col-md-6 mb-3',
                ]
            ],
            [
                'name' => 'value',
                'label' => 'Giá trị thuộc tính',
                'wrapper' => [
                    'class' => 'col-md-6 mb-3',
                ]
            ],
        ])->new_item_label('Thêm thuộc tính')->init_rows(0);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
