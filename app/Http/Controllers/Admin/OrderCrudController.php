<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Order::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order');
        CRUD::setEntityNameStrings('Đơn hàng', 'Danh sách đơn hàng');
        $this->crud->denyAccess(['create']);
    }

    public function setFilter(): void
    {
        $this->crud->addFilter([
            'name' => 'uuid',
            'type' => 'text',
            'label' => 'Mã vận đơn'
        ], false, function ($value) {
            $this->crud->query->where('uuid', "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'customer_name',
            'type' => 'text',
            'label' => 'Tên khách hàng'
        ], false, function ($value) {
            $this->crud->query->where('customer_name', "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'phone',
            'type' => 'text',
            'label' => 'Số điện thoại'
        ], false, function ($value) {
            $this->crud->query->where('phone', "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'email',
            'type' => 'text',
            'label' => 'Email'
        ], false, function ($value) {
            $this->crud->query->where('email', "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'status',
            'type' => 'select2',
            'label' => 'Trạng thái'
        ], $this->statusOption(), function ($value) {
            $this->crud->query->where('status', $value);
        });
        $this->crud->addFilter([
            'name' => 'payment_method',
            'type' => 'select2',
            'label' => 'Phương thức thanh toán'
        ], [
            'cash' => 'Thanh toán khi nhận hàng',
            'bank' => 'Chuyển khoản ngân hàng'
        ], function ($value) {
            $this->crud->query->where('payment_method', $value);
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->setFilter();
        CRUD::column('uuid')->label('Mã vận đơn');
        CRUD::column('customer_name')->label('Tên khách hàng');
        CRUD::column('phone')->type('phone');
        CRUD::column('email')->type('email');
        CRUD::column('total')->type('number')->suffix('đ')->label("Số tiền");
        CRUD::column('status')->label('Trạng thái')->type('Trạng thái')->type('select_from_array')->options($this->statusOption());
        CRUD::column('payment_method')->label('Phương thức')->type('select_from_array')->options([
            'bank' => 'Chuyển khoản ngân hàng',
            'cash' => 'Thanh toán khi nhận hàng'
        ]);
        CRUD::column('created_at')->label('Ngày tạo')->type('date');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);


        CRUD::field('status')->label('Trạng thái')->type('select_from_array')->options($this->statusOption());

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function statusOption()
    {
        return [
            0 => 'Đang chờ duyệt',
            1 => 'Đã duyệt, chờ giao hàng',
            2 => 'Đang giao hàng',
            3 => 'Đơn hàng giao thành công',
            4 => 'Đơn hàng trả về',
            5 => 'Đơn đã hủy'
        ];
    }
}
