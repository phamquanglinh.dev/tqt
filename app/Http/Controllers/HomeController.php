<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\ViewModel\Index\IndexViewModel;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function indexPageAction(): View
    {
        $pinCategory = Category::query()->orderBy('pin', "DESC")->limit(4)->get();
        $latestCategory = Category::query()->with('Products')->orderBy('pin', "DESC")->limit(3)->get();
        $indexViewModel = new IndexViewModel($pinCategory, $latestCategory);
        return \view("index", [
            'indexViewModel' => $indexViewModel
        ]);
    }
}
