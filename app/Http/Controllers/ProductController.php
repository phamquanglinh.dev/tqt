<?php

namespace App\Http\Controllers;

use App\Models\BuyItem;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use App\ViewModel\Products\ProductViewModel;

class ProductController extends Controller
{
    public function getProductBySlugAction($slug)
    {
        /**
         * @var Product $product
         */
        $product = Product::query()->where('slug', $slug)->first();

        if (!$product) {
            abort(404);
        }

        return view("single-product", [
            'productViewModel' => new ProductViewModel($product)
        ]);
    }

    public function addProductToCardAction(Request $request)
    {
        $uuid = Cookie::get('anonymous_uuid');

        if (!$uuid) {
            $uuid = Str::uuid();
            Cookie::queue('anonymous_uuid', $uuid, 60 * 24 * 365 * 10);
        }

        $request->validate([
            'product' => 'int|required',
            'quantity' => 'int|min:1|max:100'
        ]);

        $product = Product::query()->where('id', $request->get('product'))->first();

        if (!$product) {
            return redirect()->back()->withErrors([
                'product' => 'Không tìm thấy sản phẩm'
            ]);
        }

        BuyItem::query()->updateOrCreate([
            'anonymous_uuid' => $uuid,
            'product_id' => $product['id'],
            'order_id' => null
        ], [
            'anonymous_uuid' => $uuid,
            'product_id' => $product['id'],
            'product_name' => $product['name'],
            'quantity' => $request->get('quantity'),

        ]);

        return redirect()->back()->with('succeed','Đã thêm vào giỏ hàng');
    }
}
