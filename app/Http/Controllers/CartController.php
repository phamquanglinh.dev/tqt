<?php

namespace App\Http\Controllers;

use App\Models\BuyItem;
use App\Models\Order;
use App\ViewModel\Cart\CartViewModel;
use App\ViewModel\Cart\OrderViewModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;

class CartController extends Controller
{
    public function cartPageAction(): View
    {
        $uuid = Cookie::get('anonymous_uuid');

        if (!$uuid) {
            $uuid = Str::uuid();
            Cookie::queue('anonymous_uuid', $uuid, 60 * 24 * 365 * 10);
        }

        $buyItems = BuyItem::query()->where('anonymous_uuid', $uuid)->where('order_id', null)->get();


        return \view('cart', [
            'cartViewModel' => new CartViewModel($buyItems)
        ]);
    }

    public function createOrderAction(Request $request): RedirectResponse
    {
        $uuid = Cookie::get('anonymous_uuid');

        if (!$uuid) {
            abort(404);
        }

        $request->validate([
            'customer_name' => 'required|string',
            'email' => 'email|required',
            'phone' => 'string',
            'address' => 'required|string',
        ]);

        $validBuyItems = BuyItem::query()->where('anonymous_uuid', $uuid)->where('order_id', null)->get();

        if ($validBuyItems->count() < 1) {
            abort(404);
        }
        $request['uuid'] = Str::uuid();

        $order = DB::transaction(function () use ($validBuyItems, $request) {
            $order = Order::query()->create($request->only(['uuid', 'customer_name', 'email', 'phone', 'address']));
            foreach ($validBuyItems as $buyItem) {
                $buyItem->update([
                    'order_id' => $order['id']
                ]);
            }

            $cartViewModel = new CartViewModel($validBuyItems);

            $order->update([
                'total' => $cartViewModel->getTotalAmount()
            ]);

            return $order;
        });

        return redirect('/order/' . $order['uuid']);
    }

    public function orderPageAction($uuid)
    {
        /**
         * @var Order $order
         */
        $order = Order::query()->where('uuid', $uuid)->first();
        if (!$order) {
            abort(404);
        }

        $orderViewModel = new OrderViewModel($order);

        return \view('order', [
            'orderViewModel' => $orderViewModel
        ]);
    }

    public function confirmOrderPageAction(Request $request, $uuid)
    {
        $request->validate([
            'payment_method' => 'required|string|in:cash,bank'
        ]);

        $order = Order::query()->where('uuid', $uuid)->first();

        $order->update([
            'payment_method' => $request->get('payment_method')
        ]);

        return redirect('/')->with('succeed', 'Xác nhận thành công');
    }

    public function deleteItemFromCart(Request $request): RedirectResponse
    {
        $request->validate([
            'id' => 'required|integer'
        ]);

        $buyItem = BuyItem::query()->where('id', $request->get('id'))->where('order_id', null)->first();

        if (!$buyItem) {
            return redirect()->back()->with('errors', "Không tìm thấy");
        }

        $buyItem->delete();

        return redirect()->back()->with('succeed', 'Xóa thành công');
    }

    public function updateItemFromCart(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'quantity' => 'required|integer|min:1'
        ]);
        $buyItem = BuyItem::query()->where('id', $id)->where('order_id', null)->first();

        if (!$buyItem) {
            return redirect()->back()->with('errors', "Không tìm thấy");
        }

        $buyItem->update([
            'quantity' => $request->get('quantity')
        ]);
        return redirect()->back()->with('succeed', 'Cập nhật thành công');
    }
}
