<?php

namespace App\Http\Controllers;

use App\Models\Category;

use App\ViewModel\Category\CategoryViewModel;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public function getCategoryBySlugAction($slug): View
    {
        /**
         * @var Category $category
         */
        $category = Category::query()->where('slug', $slug)->first();

        if (!$category) {
            abort(404);
        }

        return \view('category',[
            'categoryViewModel' => new CategoryViewModel($category)
        ]);
    }
}
