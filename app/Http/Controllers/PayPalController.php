<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use PayPal\Api\PaymentExecution;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use Illuminate\Support\Facades\Redirect;

class PayPalController extends Controller
{
    private ApiContext $apiContext;

    public function __construct()
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENT_ID'),
                env('PAYPAL_CLIENT_SECRET')
            )
        );
    }

    public function createPayment(Request $request): RedirectResponse
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($request->input('totalAmount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription('Mô tả thanh toán');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(route('cart'))->setCancelUrl(route('paypal.cancel'));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions([$transaction])
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($this->apiContext);
            return Redirect::away($payment->getApprovalLink());
        } catch (\Exception $e) {
            return redirect()->route('cart')->with('error', $e->getMessage());
        }
    }

    public function executePayment(Request $request): RedirectResponse
    {
        $paymentId = $request->input('paymentId');
        $payerId = $request->input('PayerID');

        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            $result = $payment->execute($execution, $this->apiContext);
            return redirect()->route('paypal.success')->with('success', 'Payment successful');
        } catch (\Exception $e) {
            return redirect()->route('paypal.error')->with('error', $e->getMessage());
        }
    }

    public function cancelPayment()
    {
        // Xử lý khi người dùng hủy thanh toán
        return redirect()->route('paypal.cancel')->with('error', 'Payment cancelled');
    }
}
