<?php

namespace App\Providers;

use App\Composer\DashboardComposer;
use App\Composer\HeaderComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class ViewComposerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('components.menu', HeaderComposer::class);
        View::composer('components.dashboard', DashboardComposer::class);
    }
}
